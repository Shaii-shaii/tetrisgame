const canvas = document.getElementById('tetris');
const context = canvas.getContext('2d');

const ROW = 20;
const COL = COLUMN = 10;
const SQ = squareSize = 30;
const VACANT = "white"; //color of the empty square

//draw a square

function drawSquare(x, y, color){

    context.fillStyle = color;
    context.fillRect(x*SQ, y*SQ, SQ, SQ);
    context.strokeStyle="black";
    context.strokeRect(x*SQ, y*SQ, SQ, SQ);

}

//create the board

let board =[];
for (r=0; r<ROW; r++){
    board[r]=[];
    for(c=0; c<COL; c++){
        board[r][c] = VACANT;
    }
        
}

//draw board

function drawBoard(){
    for ( r = 0; r < ROW; r++){
        for( c = 0; c < COL; c++){
            drawSquare(c, r,board[r][c]);

}
    }
}
drawBoard();
//the pieces and the colors
let I = [
    [    [0, 0, 0, 0 ],
         [1, 1, 1, 1 ],
         [0, 0, 0, 0 ],
         [0, 0, 0, 0 ]
    ],
    [    [0, 0, 1, 0 ],
         [0, 0, 1, 0 ],
         [0, 0, 1, 0 ],
         [0, 0, 1, 0 ]
    ],
    [    [0, 0, 0, 0 ],
         [0, 0, 0, 0 ],
         [1, 1, 1, 1 ],
         [0, 0, 0, 0 ]
    ],
    [    [0, 1, 0, 0 ],
         [0, 1, 0, 0 ],
         [0, 1, 0, 0 ],
         [0, 1, 0, 0 ],
    ]
];

let J = [
    [
        [0, 1, 0],
        [0, 1, 0],
        [1, 1, 0]
    ],
    [
        [1, 0, 0],
        [1, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 1, 1],
        [0, 1, 0],
        [0, 1, 0]
    ],
    [
        [0, 0, 0],
        [1, 1, 1],
        [0, 0, 1]
    ]
];
let L = [
    [
        [0, 1, 0],
        [0, 1, 0],
        [0, 1, 1]
    ],
    [
        [0, 0, 0],
        [1, 1, 1],
        [1, 0, 0]
    ],
    [
        [1, 1, 0],
        [0, 1, 0],
        [0, 1, 0]
    ],
    [
        [0, 0, 1],
        [1, 1, 1],
        [0, 0, 0]
    ]
];
let O = [
    [0, 0, 0, 0],
    [0, 1, 1, 0],
    [0, 1, 1, 0],
    [0, 0, 0, 0]
];
let S = [
    [
        [0, 1, 1],
        [1, 1, 0],
        [0, 0, 0]
    ],
    [
        [0, 1, 0],
        [0, 1, 1],
        [0, 0, 1]
    ],
    [
        [0, 0, 0],
        [0, 1, 1],
        [1, 1, 0]
    ],
    [
        [1, 0, 0],
        [1, 1, 0],
        [0, 1, 0]
    ]
];
let Z = [
    [
        [1, 1, 0],
        [0, 1, 1],
        [0, 0, 0]
    ],
    [
        [0, 0, 1],
        [0, 1, 1],
        [0, 1, 0]
    ],
    [
        [0, 0, 0],
        [1, 1, 0],
        [0, 1, 1]
    ],
    [
        [0, 1, 0],
        [1, 1, 0],
        [1, 0, 0]
    ]
];

let T = [
    [
        [1, 1, 1],
        [0, 1, 0],
        [0, 0, 0]
    ],
    [
        [0, 0, 1],
        [0, 1, 1],
        [0, 0, 1]
    ],
    [
        [0, 0, 0],
        [0, 1, 0],
        [1, 1, 1]
    ],
    [
        [1, 0, 0],
        [1, 1, 0],
        [1, 0, 0]
    ]
];

const PIECES = [
    [Z,"red"],
    [S,"green"],
    [T,"yellow"],
    [O,"blue"],
    [L,"purple"],
    [I,"cyan"],
    [J,"orange"]
];
// generate random pieces
function randomPiece(){
    let r = randomN = Math.floor(Math.random() * PIECES.length) //0 -> 6
    return new Piece( PIECES[r][0], PIECES[r][1]);

}

let p = randomPiece();


//the object piece

function Piece(tetromino, color){
    this.tetromino = tetromino;
    this.color = color;

    this.tetrominoN = 0; //we start form the first pattern
    this.activeTetromino = this.tetromino[this.tetrominoN];

    //we need to control the pieces
    this.x = 4;
    this.y = 0;
}
//fill function
Piece.prototype.fill = function(color){
    for ( r = 0; r < this.activeTetromino.length; r++){
        for( c = 0; c < this.activeTetromino.length; c++){
            //we draw only sqaures
            if(this.activeTetromino[r][c]){
                drawSquare(this.x + c, this.y + r, color);
            }
        }
    }
}
//draw a piece to the board

Piece.prototype.draw = function(){
    this.fill(this.color);
}
//undraw a piece
Piece.prototype.unDraw = function(){
    this.fill(VACANT);
}


// move down the piece

Piece.prototype.moveDown = function(){
    if(!this.collision(0, 1, this.activeTetromino)){
        this.unDraw();
        this.y++;
        this.draw();
    }else{
        //we lock the piece and generate a new one
        p.randomPiece();
    }
    
}

//move right 
Piece.prototype.moveRight = function(){
    if(!this.collision(1, 0, this.activeTetromino)){
        this.unDraw();
        this.x++;
        this.draw();
    }
    
}

// move left
Piece.prototype.moveLeft = function(){
    if(!this.collision(-1, 0, this.activeTetromino)){
        this.unDraw();
        this.x--;
        this.draw();
    }
    
}

//rotate the piece
Piece.prototype.rotate = function(){
    let nextPattern = this.tetromino[ (this.tetrominoN + 1)%this.tetromino.length];
    let kick = 0;

    if(!this.collision(0, 0, nextPattern)){
        if(this.x > COL/2){
            //its the right wall
            kick = -1; //we need to move the piece to the left
        }else {
            //its the left wall
            kick = 1; //we need to move the piece to the right
        }
    }
    if(!this.collision(kick, 0, 0, nextPattern)){
        this.unDraw();
        this.x += kick;
        this.tetrominoN = (this.tetrominoN + 1)%this.tetromino.length; //(0+1)%4=1
        this.activeTetromino = this.tetromino[this.tetrominoN]
        this.draw();
    }
    
}
//coliision function
Piece.prototype.collision = function(x, y, piece){
    for ( r = 0; r < piece.length; r++){
        for( c = 0; c < piece.length; c++){
            //if the square is empty we skipped it
            if(!piece[r][c]){
                continue;
            }
            //coordinates of the piece after movement
            let newX = this.x + c + x;
            let newY = this.y + r + y;

            //conditions
            if(newX < 0 || newX >= COL || newY >= ROW){
                return true;
            }
            //skip newY < 0 board [-1] will crush our game
            if(newY < 0){
                continue;
            }
            //check if there is a locked piece allready in place
            if(board[newY][newX]!= VACANT){
                return true;
            }            
        }
    }
    return false;
}



//control the piece

document.addEventListener("keydown", CONTROL);

function CONTROL(event){
    if (event.keyCode==37){
        p.moveLeft();
        dropStart = Date.now();
    }
    else if (event.keyCode==38){
        p.rotate();
        dropStart = Date.now();
    }
    else if (event.keyCode==39){
        p.moveRight();
        dropStart = Date.now();
    }
    else if (event.keyCode==40){
        p.moveDown();
    }
}

// drop every second
let dropStart = Date.now();
function drop(){
    let now = Date.now();
    let delta = now - dropStart;
    if(delta > 1000){
        p.moveDown();
        dropStart = Date.now();
    }
    
    webkitRequestAnimationFrame(drop);
}
drop();